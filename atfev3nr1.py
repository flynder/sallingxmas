#!/usr/bin/env python3
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
# insert at position 1 in the path, as 0 is the path of this file.
sys.path.insert(1, '/home/robot/')

import config
client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)


p = ev3.LegoPort('outA')
assert p.connected
p.mode = 'dc-motor'
time.sleep(0.5)
m = ev3.DcMotor('outA')

trainruntime = 20

# This is the Subscriber

def on_connect(client, userdata, flags, rc):
  client.subscribe("topic/commands", qos=1)

def on_message(client, userdata, msg):
  if msg.payload.decode() == "Train Start":
#   print("Stop!")
#    time.sleep(0.5)
    status = "Train Started for " + str(trainruntime) + " seconds"
    client.publish("topic/status", status, qos=1);
    m.run_direct(duty_cycle_sp=80)
    time.sleep(trainruntime)
    m.stop()
    m.run_direct(duty_cycle_sp=-0)
    client.publish("topic/status", "Train Stopped", qos=1);


client.on_connect = on_connect
client.on_message = on_message
client.publish("topic/status", "Train Ready", qos=1);
m = ev3.DcMotor('outA')
time.sleep(0.5)
m.stop()

#while (1):
client.loop_forever()
#client.disconnect()
