#!/usr/bin/env python3
import time
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config




p = ev3.LegoPort('outA')
assert p.connected
p.mode = 'dc-motor'
p2 = ev3.LegoPort('outB')
assert p2.connected
p2.mode = 'dc-motor'
p3 = ev3.LegoPort('outC')
assert p3.connected
p3.mode = 'dc-motor'

time.sleep(0.5)
m = ev3.DcMotor('outA')
m2 = ev3.DcMotor('outB')
m3 = ev3.DcMotor('outC')

def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("topic/commands", qos=1)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "train1 on":
      print("train1 on")
      m.run_direct(duty_cycle_sp=-60)
      status = "Train1 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train1 off":
      print("train1 off")
      m.stop()
      m.run_direct(duty_cycle_sp=-0)
      client.publish("topic/status", "Train1 Stopped", qos=1);   
   if msg.payload.decode() == "train2 on":
      print("train2 on")
      m2.run_direct(duty_cycle_sp=50)
      status = "Train2 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train2 off":
      print("train2 off")
      m2.stop()
      m2.run_direct(duty_cycle_sp=-0)
      client.publish("topic/status", "Train2 Stopped", qos=1);   
   if msg.payload.decode() == "train3 on":
      print("train3 on")
      m3.run_direct(duty_cycle_sp=-85)
      status = "Train3 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train3 off":
      print("train3 off")
      m3.stop()
      m3.run_direct(duty_cycle_sp=-0)
      client.publish("topic/status", "Train3 Stopped", qos=1);   

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

time.sleep(0.5)
m.stop()
m2.stop()
m3.stop()

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
