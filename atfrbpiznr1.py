#!/usr/bin/env python3
import board
import neopixel
import time
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config
import requests
import json
import os

# This is the Publisher

pixels = neopixel.NeoPixel(board.D18, 30)


pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 50

ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)
pixels.fill((0, 0, 0))
pixels.show()
pixels.fill((0, 0, 0))
pixels.show()
pixels.fill((0, 0, 0))
pixels.show()
time.sleep(1)

def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("topic/commands", qos=0)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "aros on":
      print("aros on")
      for x in range(20, 50):
         pixels[x] = (255, 255, 255)
         time.sleep(0.1)
      pixels.show()
   if msg.payload.decode() == "aros off":
      print("aros off")
      for x in range(20, 50):
         pixels.fill((0, 0, 0))
      pixels.show()
   if msg.payload.decode() == "police on":
      print("police on")
      client.publish("topic/status", "Police turned on", qos=0);   
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.102 on")
   if msg.payload.decode() == "police off":
      print("police off")
      client.publish("topic/status", "Police turned off", qos=0);  
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.102 off")
   if msg.payload.decode() == "salling on":
      print("salling on")
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.149 on")
   if msg.payload.decode() == "salling off":
      print("tree off")
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.149 off")
   if msg.payload.decode() == "tree on":
      print("tree on")
      client.publish("topic/status", "Tree turned on", qos=0);   
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.116 on")
   if msg.payload.decode() == "tree off":
      print("tree off")
      client.publish("topic/status", "Tree turned off", qos=0);   
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.116 off")
   if msg.payload.decode() == "caves on":
      print("caves on")
      client.publish("topic/status", "Caves turned on", qos=0); 
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.198 on")
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.103 on")
   if msg.payload.decode() == "caves off":
      print("caves off")
      client.publish("topic/status", "Caves turned off", qos=0); 
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.198 off")
      os.system("/home/pi/sallingxmas/hs100.sh -i 192.168.0.103 on")
      
client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,20)


client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()