#!/usr/bin/env python3
import time
import paho.mqtt.client as mqtt
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config
import requests
import json



p = ev3.LegoPort('outA')
assert p.connected
p.mode = 'dc-motor'
time.sleep(0.5)
m = ev3.DcMotor('outA')

def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("topic/commands", qos=0)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "train4 on":
      print("train4 on")
      m.run_direct(duty_cycle_sp=50)
      status = "Train4 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train4 off":
      print("train4 off")
      m.stop()
      m.run_direct(duty_cycle_sp=-0)
      client.publish("topic/status", "Train Stopped", qos=1);   

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

m = ev3.DcMotor('outA')
time.sleep(0.5)
m.stop()


client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
