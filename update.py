#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import socket
import sys
# insert at position 1 in the path, as 0 is the path of this file.
sys.path.insert(1, '/home/robot/')
import config

# This is the Publisher

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)
text = socket.gethostname() + "is alive"
client.publish("topic/status", text, qos=1);
client.disconnect();
